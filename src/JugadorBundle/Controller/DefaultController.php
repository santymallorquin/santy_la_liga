<?php

namespace JugadorBundle\Controller;

use AppBundle\Entity\Jugador;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\MainController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/*
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\ORM\Query;
*/

class DefaultController extends Controller
{
    public function indexAction()
    {
        $allPlayers = $this->getAllPlayers();

        $response = new Response($allPlayers);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function jugadoresAction()
    {
        return $this->render("@Jugador/Default/jugadores.html.twig");
    }

    public function getAllPlayers()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador');
        $jugadores = $repository->findAll();

        $serializer = $this->container->get('jms_serializer');
        $jugadoresJson = $serializer->serialize($jugadores, 'json');

        return $jugadoresJson;

    }

    public function getJugadoresPorEquipoAction($id_club)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador');

        $jugadores = $repository->findBy(array('idClub' => $id_club));

        return $this->render("@Jugador/Default/jugadoresPorClub.html.twig"
            , array(
                'jugadores' => $jugadores,
                'idClub' => $id_club
            )
        );
    }

    public function crearJugadorAction($nombre_jugador, $id_club)
    {
        $jugador = new Jugador();
        $jugador->setNombreJugador($nombre_jugador);
        $jugador->setIdClub($id_club);

        $em = $this->getDoctrine()->getManager();

        $em->persist($jugador);
        $em->flush();

        return new RedirectResponse($this->generateUrl('clubDetalle', array('id_club' => $id_club)));
    }
}
