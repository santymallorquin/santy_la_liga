<?php

namespace ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function mainAction()
    {

        return $this->render("@Club/Default/main.html.twig");
    }

    public function indexAction()
    {
        $clubes = $this->getAllTeams();

        $response = new Response($clubes);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function clubesAction()
    {
        return $this->render("@Club/Default/clubes.html.twig");
    }

    public function getClubInfoAction($id_club)
    {

        $response = $this->forward('JugadorBundle:Default:getJugadoresPorEquipo'
            , array(
                'id_club' => $id_club
            )
        );

        return $response;
    }

    public function getOptionsClubesAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Club');
        $clubes = $repository->findAll();

        //dump($clubes);

        $html = '';
        foreach ($clubes AS $club) {
            $html .= '<option value="';
            $html .= $club->getIdClub();
            $html .= '">';
            $html .= $club->getNombreClub();
            $html .= '</option>';
        }

        //dump($html);
        $response = new Response($html);

        return $response;
    }

    public function getAllTeams($format = 'json')
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Club');
        $clubes = $repository->findAll();

        if ($format == 'json') {
            $serializer = $this->container->get('jms_serializer');
            $clublesJson = $serializer->serialize($clubes, 'json');

            return $clublesJson;
        } else {
            return $clubes;
        }
    }
}
