<?php

namespace AppBundle\Entity;

/**
 * Club
 */
class Club
{
    /**
     * @var integer
     */
    private $idClub;

    /**
     * @var string
     */
    private $nombreClub;


    /**
     * Get idClub
     *
     * @return integer
     */
    public function getIdClub()
    {
        return $this->idClub;
    }

    /**
     * Set nombreClub
     *
     * @param string $nombreClub
     *
     * @return Club
     */
    public function setNombreClub($nombreClub)
    {
        $this->nombreClub = $nombreClub;

        return $this;
    }

    /**
     * Get nombreClub
     *
     * @return string
     */
    public function getNombreClub()
    {
        return $this->nombreClub;
    }
}

