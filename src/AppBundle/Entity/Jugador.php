<?php

namespace AppBundle\Entity;

/**
 * Jugador
 */
class Jugador
{
    /**
     * @var integer
     */
    private $idJugador;

    /**
     * @var string
     */
    private $nombreJugador;

    /**
     * @var integer
     */
    private $idClub;


    /**
     * Get idJugador
     *
     * @return integer
     */
    public function getIdJugador()
    {
        return $this->idJugador;
    }

    /**
     * Set nombreJugador
     *
     * @param string $nombreJugador
     *
     * @return Jugador
     */
    public function setNombreJugador($nombreJugador)
    {
        $this->nombreJugador = $nombreJugador;

        return $this;
    }

    /**
     * Get nombreJugador
     *
     * @return string
     */
    public function getNombreJugador()
    {
        return $this->nombreJugador;
    }

    /**
     * Set idClub
     *
     * @param integer $idClub
     *
     * @return Jugador
     */
    public function setIdClub($idClub)
    {
        $this->idClub = $idClub;

        return $this;
    }

    /**
     * Get idClub
     *
     * @return integer
     */
    public function getIdClub()
    {
        return $this->idClub;
    }
}

