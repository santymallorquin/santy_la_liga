<?php
/**
 * Created by PhpStorm.
 * User: Santy
 * Date: 05/04/2018
 * Time: 20:53
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MainController
{

    /**
     * @Route("/inicio", name="homepage")
     * @return Response
     */
    public function mainAction()
    {
        return new Response('holiii');
    }

    public function getHeader()
    {
        return 'header';
    }

    public function getFooter()
    {
        return 'footer';
    }
}