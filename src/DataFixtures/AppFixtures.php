<?php
namespace App\DataFixtures;

use AppBundle\Entity\Jugador;
use AppBundle\Entity\Club;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // crea 20 club!
        for ($i = 0; $i < 20; $i++) {
            $product = new Club();
            $product->setNombreClub('Equipo' . $i);
        }

        // crea 100 jugadores!
        for ($i = 0; $i < 100; $i++) {
            $product = new Jugador();
            $product->setIdClub(rand(1, 20));
            $product->setNombreJugador('jugador' . $i);
        }

        $manager->flush();
    }
}
