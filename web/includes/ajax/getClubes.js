var url = 'http://127.0.0.1:8000/club';
$.ajax({
    url: url,
    dataType: 'json',
    type: 'GET',
    error: function (data) {
        console.log(data);
        alert('Ha habido un error al cargar los datos.');
    },
    success: function (data) {
        console.log(data);
        $('#equipos').bootstrapTable({
            data: data
        });

        /*$('#equipos > tbody  > tr > td:first-child').each(function() {
            var id = $(this).text();
            console.log('id => ' + id);
            console.log($(this));

        });*/
        $('td:first-child').each(function(){
            var $td = $(this);
            $td.html('<a href="/equipo/'+$(this).text()+'"> Ver equipo</a>');
        });
    }
});