//cambiar a ruta relativa
var url = 'http://127.0.0.1:8000/jugador';
$.ajax({
    url: url,
    dataType: 'json',
    type: 'GET',
    error: function () {
        alert('Ha habido un error al cargar los datos jugador.');
    },
    success: function (data) {
        $('#table').bootstrapTable({
            data: data
        });
    }
});