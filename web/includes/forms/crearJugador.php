<?php
/* CREAR EL FORM CON SYMFONY */
$nombreJugador = '';
$idClub = '';
if (isset($_POST['nombreJugador'])) {
    $nombreJugador = $_POST['nombreJugador'];
}

if (isset($_POST['clubJugador'])) {
    $idClub = $_POST['clubJugador'];
}

if ($nombreJugador != '' && $idClub != '') {
    header('Location: /crearJugador/'.$nombreJugador.'/'.$idClub);
}else{
    header('Location: /');
}